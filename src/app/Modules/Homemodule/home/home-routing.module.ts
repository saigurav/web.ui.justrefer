import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BodycontainerComponent } from 'src/app/JR/Body/bodycontainer/bodycontainer.component';


const routes: Routes = [
  {path:'',component:BodycontainerComponent,
  children:[{path:'',component:BodycontainerComponent}]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }

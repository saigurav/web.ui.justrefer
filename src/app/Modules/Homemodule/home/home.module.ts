import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { BodycontainerComponent } from 'src/app/JR/Body/bodycontainer/bodycontainer.component';
import { SideMenuComponent } from 'src/app/JR/Body/side-menu/side-menu.component';


@NgModule({
  declarations: [BodycontainerComponent,SideMenuComponent],
  imports: [
    CommonModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }

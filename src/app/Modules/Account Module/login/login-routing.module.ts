import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from 'src/app/Account/login/login.component';

const routes: Routes = [
  {path:'',component:LoginComponent},
  {path:'home',loadChildren:()=>import("src/app/Modules/Homemodule/home/home.module").then(m=>m.HomeModule)},
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }

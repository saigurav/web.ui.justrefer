import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BodyComponent } from 'src/app/Layout/body/body.component';

const routes: Routes = [
  {path:'',component:BodyComponent,
   children:[{path:'',loadChildren:()=>import("src/app/Modules/Account Module/login/login.module").then(m=>m.LoginModule)}] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BodyRoutingModule { }

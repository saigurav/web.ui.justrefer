import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BodyRoutingModule } from './body-routing.module';
import { BodyComponent } from 'src/app/Layout/body/body.component';
import { HeaderComponent } from 'src/app/Layout/header/header.component';
import { FooterComponent } from 'src/app/Layout/footer/footer.component';


@NgModule({
  declarations: [
    BodyComponent,HeaderComponent,FooterComponent
  ],
  imports: [
    CommonModule,
    BodyRoutingModule
  ]
})
export class BodyModule { }
